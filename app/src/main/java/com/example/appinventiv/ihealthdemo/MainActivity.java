package com.example.appinventiv.ihealthdemo;

import android.Manifest;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.ihealth.communication.control.Am4Control;
import com.ihealth.communication.control.AmProfile;
import com.ihealth.communication.manager.iHealthDevicesCallback;
import com.ihealth.communication.manager.iHealthDevicesManager;
import com.pryv.Connection;
import com.pryv.Filter;
import com.pryv.database.DBinitCallback;
import com.pryv.interfaces.StreamsCallback;
import com.pryv.model.Stream;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {
    private TextView tvSteps, tvCalories, tvSleep;
    private Button btnConnectDevice;
    private ProgressBar pbConnection;
    private int callbackId;
    private Class selectedDeviceClass;
    private String selectedDeviceType;
    private final static int BLUETOOTH_ENABLED = 1;
    private boolean bluetoothReady = false;
    private Connection connection;
    private Stream stepsStream;
    private Stream activitiesStream;
    private Stream sleepStream;
    private StreamsCallback streamsCallback;
    private Am4Control am4Control;
    private iHealthDevicesCallback iHealthDevicesCallback;


    private String userName = "tarish.goyal@mutelcor.com";
    private String clientId = "b79568ff9f424d9eb1c4a0273e3f0c15";
    private String clientSecret = "53080b9eeb2b4253a75b0de5e8af81ec";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        iHealthDevicesManager.getInstance().init(MainActivity.this);
        inittializeViews();
        setListeneres();
        checkBluetooth();
        callbackId = iHealthDevicesManager.getInstance().registerClientCallback(new iHealthDevicesCallback() {
            @Override
            public void onScanDevice(String mac, String deviceType) {
                if (selectedDeviceType.equals(deviceType)) {
                    iHealthDevicesManager.getInstance().stopDiscovery();
                    iHealthDevicesManager.getInstance().connectDevice(userName, mac);
                }
            }


            @Override
            public void onDeviceConnectionStateChange(String mac, String deviceType, int status) {
                if (selectedDeviceType.equals(deviceType) && status == 1) {
                    getDataFromIhealthdevice(mac);
                }
            }

            @Override
            public void onUserStatus(String username, int userStatus) {
            }

            @Override
            public void onDeviceNotify(String mac, String deviceType, String action, String message) {
                switch (action) {
                    case AmProfile.ACTION_SYNC_SLEEP_DATA_AM:
                        try {
                            //TODO: time, activity with duration
                            JSONObject info = new JSONObject(message);
                            JSONArray sleep_info = info.getJSONArray(AmProfile.SYNC_SLEEP_DATA_AM);
                            JSONArray activities = sleep_info.getJSONObject(0).getJSONArray(AmProfile.SYNC_SLEEP_EACH_DATA_AM);

                           /* tv_return.setText("Sync " + activities.length() + " sleeps...");

                            for (int i = 0; i < activities.length(); i++) {
                                JSONObject activity = activities.getJSONObject(i);
                                String time = activity.getString(AmProfile.SYNC_SLEEP_DATA_TIME_AM);
                                String level = activity.getString(AmProfile.SYNC_SLEEP_DATA_LEVEL_AM);
                                if (connection != null) {
                                    double unixTime = getUnixTime(time);
                                    Event e = new Event(sleepStream.getId(), "count/generic", level);
                                    e.setTime(unixTime);
                                    connection.events.create(e, eventsCallback);
                                }
                            }*/
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case AmProfile.ACTION_SYNC_ACTIVITY_DATA_AM:
                        try {
                            JSONObject info = new JSONObject(message);
                            JSONArray activity_info = info.getJSONArray(AmProfile.SYNC_ACTIVITY_DATA_AM);
                            JSONArray activities = activity_info.getJSONObject(0).getJSONArray(AmProfile.SYNC_ACTIVITY_EACH_DATA_AM);

                            for (int i = 0; i < activities.length(); i++) {
                                JSONObject activity = activities.getJSONObject(i);
                              /*  String time = activity.getString(AmProfile.SYNC_ACTIVITY_DATA_TIME_AM);
                                String stepLength = activity.getString(AmProfile.SYNC_ACTIVITY_DATA_STEP_LENGTH_AM);
                                String steps = activity.getString(AmProfile.SYNC_ACTIVITY_DATA_STEP_AM);
                                String calories = activity.getString(AmProfile.SYNC_ACTIVITY_DATA_CALORIE_AM);*/
                                tvCalories.setText("Synced " + activity.getString(AmProfile.SYNC_ACTIVITY_DATA_STEP_AM) + " stpes " + activity.getString(AmProfile.SYNC_ACTIVITY_DATA_CALORIE_AM) + " calories ");
                               /* if (connection != null) {
                                    double unixTime = getUnixTime(time);
                                    Event e1 = new Event(activitiesStream.getId(), "time/min", stepLength);
                                    e1.setTime(unixTime);
                                    Event e2 = new Event(activitiesStream.getId(), "count/steps", steps);
                                    e1.setTime(unixTime);
                                    Event e3 = new Event(activitiesStream.getId(), "energy/cal", calories);
                                    e1.setTime(unixTime);
                                    connection.events.create(e1, eventsCallback);
                                    connection.events.create(e2, eventsCallback);
                                    connection.events.create(e3, eventsCallback);
                                }*/
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case AmProfile.ACTION_SYNC_REAL_DATA_AM:
                        try {
                            JSONObject info = new JSONObject(message);
                            String real_info = info.getString(AmProfile.SYNC_REAL_STEP_AM);
                            tvSteps.setText("Real steps: " + info.getString(AmProfile.SYNC_REAL_STEP_AM) + " Real calories: " + info.getString(AmProfile.SYNC_REAL_CALORIE_AM));
                          /*  if (connection != null) {
                                connection.events.create(new Event(stepsStream.getId(), "count/generic", real_info), eventsCallback);
                            }*/
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    default:
                        break;
                }
            }

            @Override
            public void onScanFinish() {
                pbConnection.setVisibility(View.GONE);
            }
        });

        iHealthDevicesManager.getInstance().sdkUserInAuthor(MainActivity.this, userName, clientId, clientSecret, callbackId);
    }


    /*
    * method to set listeners to views;
    *
    * */
    private void setListeneres() {
        btnConnectDevice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkPermissions();
            }
        });
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        iHealthDevicesManager.getInstance().stopDiscovery();
        iHealthDevicesManager.getInstance().unRegisterClientCallback(callbackId);
        iHealthDevicesManager.getInstance().destroy();
    }


    /*
    *
    * method to check bluetooth
    * */
    private void checkBluetooth() {
        BluetoothAdapter bluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (bluetoothAdapter == null) {
            // Device does not support Bluetooth
            pbConnection.setVisibility(View.VISIBLE);
        } else if (!bluetoothAdapter.isEnabled()) {
            // Bluetooth is not enable
            Intent enableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, BLUETOOTH_ENABLED);
        } else {
            bluetoothReady = true;
        }
    }


/*
*
* method to discover ihealth devices
* */

    private void discover(String type, int discoverType) {
        if (bluetoothReady) {
            pbConnection.setVisibility(View.VISIBLE);
            selectedDeviceType = type;
            iHealthDevicesManager.getInstance().startDiscovery(discoverType);
        } else {
            Toast.makeText(this, "Bluetooth is not ready!", Toast.LENGTH_SHORT).show();
        }
    }

    /*
    *
    * method to initialize views
    * */
    private void inittializeViews() {
        tvSteps = (TextView) findViewById(R.id.tv_steps);
        tvCalories = (TextView) findViewById(R.id.tv_calories);
        tvSleep = (TextView) findViewById(R.id.tv_sleep);
        btnConnectDevice = (Button) findViewById(R.id.btn_connect_device);
        pbConnection = (ProgressBar) findViewById(R.id.pb_connection);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Check which request we're responding to
        switch (requestCode) {
            case BLUETOOTH_ENABLED:
                // Make sure the request was successful
                bluetoothReady = (resultCode == RESULT_OK);
                break;
        }
    }


    /*
 *
 * method to get data from iHealth device
 * */
    private void getDataFromIhealthdevice(String mac) {
        setCallbacks();
        connection = new Connection(this, userName, null, null, true, new DBinitCallback());
        stepsStream = new Stream("AM4_realSteps", "AM4_realSteps");
        activitiesStream = new Stream("AM4_syncActivities", "AM4_syncActivities");
        sleepStream = new Stream("AM4_syncSleeps", "AM4_syncSleeps");

        Filter scope = new Filter();
        scope.addStream(stepsStream);
        scope.addStream(activitiesStream);
        scope.addStream(sleepStream);
        connection.setupCacheScope(scope);
        connection.streams.create(stepsStream, streamsCallback);
        connection.streams.create(activitiesStream, streamsCallback);
        connection.streams.create(sleepStream, streamsCallback);

        iHealthDevicesManager.getInstance().addCallbackFilterForDeviceType(callbackId, iHealthDevicesManager.TYPE_AM4);
        am4Control = iHealthDevicesManager.getInstance().getAm4Control(mac);
       // am4Control.syncActivityData();
          am4Control.syncRealData();
        //   am4Control.syncSleepData();
    }


    /*
    * method to set callbacks
    *
    * */
    private void setCallbacks() {
        streamsCallback = new StreamsCallback() {

            @Override
            public void onApiSuccess(String s, Stream stream, Double aDouble) {
                Log.i("Pryv", s);
            }

            @Override
            public void onApiError(String s, Double aDouble) {
                Log.e("Pryv", s);
            }

            @Override
            public void onCacheSuccess(String s, Stream stream) {
                Log.i("Pryv", s);
            }

            @Override
            public void onCacheError(String s) {
                Log.e("Pryv", s);
            }

        };
    }
    
    
    
    /*
    *
    * method to check permissions
    * */

    private void checkPermissions() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.RECORD_AUDIO}, 1);
                        } else {
                            ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                        }
                    } else {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE}, 1);
                    }
                } else {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
                }
            } else if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.RECORD_AUDIO}, 1);
                    } else {
                        ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                    }
                } else {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.READ_PHONE_STATE}, 1);
                }
            } else if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.RECORD_AUDIO}, 1);
                } else {
                    ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 1);
                }
            } else if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.RECORD_AUDIO) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(MainActivity.this, new String[]{Manifest.permission.RECORD_AUDIO}, 1);
            } else {
                callToDeviceDiscover();
            }
        } else {
            callToDeviceDiscover();
        }

    }


    /*
    *
    * method to call dicover of iHealth device after  marshmallow permission check
    * */
    private void callToDeviceDiscover() {
        tvCalories.setText("");
        discover(iHealthDevicesManager.TYPE_AM4, iHealthDevicesManager.DISCOVERY_AM4);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        //   Log.e("inapp", "permission");
        switch (requestCode) {
            case 1:
                if (grantResults.length == 4) {
                    if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
                        if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.READ_PHONE_STATE) == PackageManager.PERMISSION_GRANTED) {
                            if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                                if (ContextCompat.checkSelfPermission(MainActivity.this, Manifest.permission.RECORD_AUDIO) == PackageManager.PERMISSION_GRANTED) {
                                    callToDeviceDiscover();
                                }
                            }
                        }
                    }
                } else {
                    Toast.makeText(MainActivity.this, "Permissions reuired to discover iHealth device.", Toast.LENGTH_LONG).show();
                }
                break;
        }
    }


}
